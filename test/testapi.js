//Carga librerias de test, framework y añadidos
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

//hacrr aserciones
//should -> asserciones tipo BDD
//expect es referencia a una función
var should = chai.should();
//Se esta ejecutando el servidor para levantarlo de forma autocontenido al test
var server = require('../server');

//los test se  agrupan en test suite qque tengan relación
//para hacer suites se usa describe
//agrupar test en una suite
describe('First unit test',
  function(){
      //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
      it('Test that google works', function(done) {
          // Hacer petición http
          //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')
          chai.request('http://www.google.com')
          //identificación del PATH y haz un GET Petición asincrona, cuando el servidor responda vamos a END
          .get('/')
          // ejecución cuando vuelve la petición
          .end(
              function(err,res){
                console.log("Request has Finished!!");
                //Comentar las dos siguientes lineas para quitar verbose
                //console.log(err);
                //console.log(res);
                //identifica cuando termina la ejeción y tiene que empezar a comprobar aserciones y sube a comprobarlas
                //comprobar que el objeto tenga un elemento status con valor de 2000
                // Hay que probar test negativo además del positivo
                res.should.have.status(200);
                done();
              }
          )
        }
      )
  }
)

describe('Test API GET v1 de Usuarios V1',
  function(){
      it('Prueba que el API de Usuarios Responde', function(done) {
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
              function(err,res){
                console.log("Request has Finished!!");
                //console.log(err);
                //console.log(res);
                res.should.have.status(200);
                done();
              }
          )
        }
      ), it('Prueba que el API GET de Usuarios Devuelve una Lista Correcta', function(done) {
          //Que es una lista de usuarios correcta
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
              function(err,res){
                console.log("Request has Finished!!");
                //Por cada test debería haber una aserción o comprobación
                res.should.have.status(200);
                res.body.users.should.be.a("array");

                for(user of res.body.users){
                  user.should.have.property('first_name');
                  user.should.have.property('last_name');
                  user.should.have.property('email');
                  user.should.have.property('password');
                }

                done();
              }
          )
        }
      ), it('Prueba que el API GET de Usuarios Devuelve una Lista Correcta y COUNT', function(done) {
          //Que es una lista de usuarios correcta
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users?$count=true')
          .end(
              function(err,res){
                console.log("Request has Finished!!");
                //Por cada test debería haber una aserción o comprobación
                res.should.have.status(200);
                var lcount=res.body.count;
                //console.log(res.body);
                console.log(lcount);
                lcount.should.equal(res.body.users.length);

                res.body.users.should.be.a("array");
                for(user of res.body.users){
                  user.should.have.property('first_name');
                  user.should.have.property('last_name');
                  user.should.have.property('email');
                  user.should.have.property('password');
                }

                done();
              }
          )
        }
      ), it('Prueba que el API GET de Usuarios Devuelve una Lista Correcta y TOP', function(done) {
          //Que es una lista de usuarios correcta
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users?$top=2')
          .end(
              function(err, res){
                console.log("Request has Finished!!");
                //Por cada test debería haber una aserción o comprobación
                res.should.have.status(200);
                var lcount=res.body.users.length;
                console.log(lcount);
                lcount.should.equal(2);

                res.body.users.should.be.a("array");
                for(user of res.body.users){
                  user.should.have.property('first_name');
                  user.should.have.property('last_name');
                  user.should.have.property('email');
                  user.should.have.property('password');
                }

                done();
              }
          )
        }
      ) , it('Prueba que el API GET de Usuarios Devuelve una Lista Correcta y COUNT y TOP', function(done) {
          //Que es una lista de usuarios correcta
          chai.request('http://localhost:3000')
          .get('/apitechu/v1/users?$top=2&$count=true')
          .end(
              function(err, res){
                console.log("Request has Finished!!");
                //Por cada test debería haber una aserción o comprobación
                res.should.have.status(200);
                res.body.count.should.not.equals(0);
                res.body.users.length.should.equal(2);

                res.body.users.should.be.a("array");
                for(user of res.body.users){
                  user.should.have.property('first_name');
                  user.should.have.property('last_name');
                  user.should.have.property('email');
                  user.should.have.property('password');
                }

                done();
              }
          )
        }
      )
    }
)

let user_details = {
  "id": 50,
  "first_name": "FName Test",
  "last_name": "LName_test",
  "email": "email@test.com",
  "password": "123abc"
};

describe('Test API POST V1 Usuario',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test POST Usuario v1', function(done) {
      chai.request('http://localhost:3000')
      .post('/apitechu/v1/users')
      .send(user_details)
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          var users = require('../users_pwd.json');
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          console.log("POST="+users.length);
          users[users.length-1].should.have.property('password');
          users[users.length-1].email.should.equal(user_details.email);
          done();
        }
      )
    }
  )
  }
)

describe('Test API DELETE V1 Usuario',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test DELETE Usuario v1', function(done) {
      chai.request('http://localhost:3000')
      .delete('/apitechu/v1/users/50')
      .end(
        function(err, res){
          console.log("Request has Finished!!");
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          var users = require('../users_pwd.json');
          console.log("DEL="+users.length);
          done();
        }
      )
    }
  )
  }
)

describe('Test API GET V2 Usuario',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test GET Usuario v2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/')
      .end(
        function(err, res,body){
          console.log("Request GET V2 has Finished!!");
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          console.log("Num users="+res.body.length);
          //console.log(res.body);
          res.body.should.be.a("array");
          for(var data of res.body){
            data.should.have.property('_id');
            data.should.have.property('first_name');
          //  data.should.have.property('last_name');
            data.should.have.property('email');
            data.should.have.property('password');
          }
          done();
        }
      )
    }
  )
  }
)

describe('Test API GET User BY ID V2 ',
  function(){
    //test unitario 'it()' cuando vayamos metiento el test unitario va saliendo info '' de it
    it('Test GET User BY ID V2', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v2/users/2')
      .end(
        function(err, res, body){
          console.log("Request GET User By ID V2 has Finished!!");
          //Por cada test debería haber una aserción o comprobación
          res.should.have.status(200);
          res.body.first_name.should.equal('Pegeen');
          res.body.last_name.should.equal('Faulconbridge');
          res.body.email.should.equal('pfaulconbridge1@eepurl.com');
          res.body.password.should.equal('Ak2GSta85P');
          done();
        }
      )
    }
  )
  }
)
