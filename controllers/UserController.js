const io = require('../io');
const crypt = require('../crypt');
//concatenación de formación de URL
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=GPWQjLzdeSrm_lHclDEyx345ZWH7LOeL";
const requestJson = require('request-json');

//GET USERS
function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");
  console.log(req.query);

  var result = {};
  var users = require('../users_pwd.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
     users.slice(0, req.query.$top) : users;

  res.send(result);
}

//CREATE USER V1
function createUserV1(req, res){ // funcion manejadora
  console.log("POST /apitechu/v1/users");
  //Coger datos de cabecera, normamel para datos de configuración
  console.log("id ="+req.body.id);
  console.log("first_name ="+req.body.first_name);
  console.log("last_name ="+req.body.last_name);
  console.log("email ="+req.body.email);
  console.log("password ="+req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  };

  var users = require ('../users_pwd.json');
  users.push(newUser);

  //REFACTORIZADO
  io.writeUserDataToFile(users);

  console.log("Usuario añadido con éxito");
  //TRAMPA
  //res.send(users);
  res.send({"msg": "Usuario añadido con éxisto!"});
  //TRAMPA

}

//DELETE USERS
function deleteUserV1(req, res){ // funcion manejadora
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("El ID es = " + req.params.id);
  // Array con los usuarios
  var users = require('../users_pwd.json');
  //funcionaes de manipulación de listas (splice)
  //Recomendable para arrays no my grandes
  //Se usa para soluciones de mapeo
  users.forEach(function (user, index) {
    if (user.id == req.params.id) {
      console.log("La id coincide");
      users.splice(index, 1);
      io.writeUserDataToFile(users);
      res.send({"msg": "Usuario borrado con éxisto!"});
    }
  });
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Created");
  console.log(baseMLabURL);
  console.log(baseMLabURL+"user?"+mLabAPIkey);
  httpClient.get("user?"+mLabAPIkey,
    function(err,resMLab, body) {
      var response = ! err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Created");
  console.log(baseMLabURL+"user?" + query +"&"+ mLabAPIkey);
  httpClient.get("user?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "msg" : "Error obteniendo usuarios"
        };
        res.status(500);
      } else {
        if(body.length > 0) {
          response = body[0];
        } else {
          var response={
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

//CREATE USER V2
function createUserV2(req, res){ // funcion manejadora
  console.log("POST /apitechu/v2/users");
  //Coger datos de cabecera, normamel para datos de configuración
  console.log("id ="+req.body.id);
  console.log("first_name ="+req.body.first_name);
  console.log("last_name ="+req.body.last_name);
  console.log("email ="+req.body.email);
  console.log("password ="+req.body.password);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "lasta_name" : req.body.last_name,
    "email" : req.body.email,
  //encriptar password con libreria bcrypt lo ponemos en otro modulo los temas de criptografia
    "password" : crypt.hash(req.body.password)
  };


  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Created");
  console.log(baseMLabURL+"user?"+ mLabAPIkey);
  httpClient.post("user?" + mLabAPIkey, newUser,
    function(err,resMLab, body) {
      console.log("User Created");
      res.send({"msg" : "User Created"})
    }
  );
}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
