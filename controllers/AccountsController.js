//concatenación de formación de URL
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucgt6ed/collections/";
const mLabAPIkey = "apiKey=GPWQjLzdeSrm_lHclDEyx345ZWH7LOeL";
const requestJson = require('request-json');

function getAccountsV2(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"USERID" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client Created");
  console.log(baseMLabURL+"accounts?" + query +"&"+ mLabAPIkey);
  httpClient.get("accounts?"+ query +"&"+ mLabAPIkey,
    function(err,resMLab, body) {
      if(err){
        var response={
          "msg" : "Error obteniendo usuarios"
        };
        res.status(500);
      } else {
        if(body.length > 0) {
          response = body[0];
        } else {
          var response={
            "msg" : "Accounts no encontrado"
          };
          res.status(404);
        }
      }
      res.send(response);
    }
  );
}

module.exports.getAccountsV2 = getAccountsV2;
